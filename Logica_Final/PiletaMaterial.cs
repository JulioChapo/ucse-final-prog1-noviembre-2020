﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica_Final
{
    class PiletaMaterial: Pileta
    {
        public decimal Largo { get; set; }
        public decimal Ancho { get; set; }
        public decimal Profundidad { get; set; }
        public bool Trampolin { get; set; }
        public int CantidadDeEscaleras { get; set; }

        public override decimal CalcularPorcentajeDescuento()
        {
            if (Profundidad > (decimal)1.5 && Trampolin== true)
            {
                return 10 * Precio / 100;
            }
            return 5 * Precio / 100;
        }
    }
}
