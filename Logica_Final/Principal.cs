﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica_Final
{
    class Principal
    {
        List<Pileta> Piletas = new List<Pileta>();
        List<Venta> Ventas = new List<Venta>();
        List<Persona> Personas = new List<Persona>();

        //CORRECCIÓN: NO ES CORRECTO.
        public string DevolverNota(string nota)
        {
            return nota;
        }


        public bool RegistrarCompra (int dni, int codigoPileta)
        {
            Venta nuevaVentas = new Venta();
            nuevaVentas.DNIComprador = dni;
            nuevaVentas.NumeroVenta += 1;
            nuevaVentas.FechaVenta = DateTime.Today;

            foreach (var pileta in Piletas)
            {
                foreach (var persona in Personas)
                {
                    if (dni == persona.DNI && persona is Empleado)
                    {
                        nuevaVentas.ImporteTotal = pileta.Precio - pileta.CalcularPorcentajeDescuento();
                        Ventas.Add(nuevaVentas);
                        //CORRECCIÓN: SE DEBE RETORNAR AMBOS VALORES A LA VEZ, ADEMÁS UN MÉTODO DEBE TENER UN ÚNICO TIPO DE DATOS A RETORNAR, ES DECIR, NO PUEDO RETORNAR UN BOOL Y LUEGO UN STRING.
                        //ADEMÁS NO SE USA EL VALOR DEL MÉTODO DevolverNota
                        DevolverNota("El registro se realizo correctamente.");
                        return true;
                    }
                    else
                    if (dni == persona.DNI && persona is Cliente)
                    {
                        Cliente Clientes = persona as Cliente;
                        Clientes.FechaInstalacion = DateTime.Today;
                        Clientes.CodigoPiletaCliente = codigoPileta;
                        nuevaVentas.ImporteTotal = pileta.Precio - pileta.CalcularPorcentajeDescuento();
                        Ventas.Add(nuevaVentas);
                        //CORRECCIÓN: SE DEBE RETORNAR AMBOS VALORES A LA VEZ, ADEMÁS UN MÉTODO DEBE TENER UN ÚNICO TIPO DE DATOS A RETORNAR, ES DECIR, NO PUEDO RETORNAR UN BOOL Y LUEGO UN STRING.
                        //ADEMÁS NO SE USA EL VALOR DEL MÉTODO DevolverNota
                        DevolverNota("Se registro la nueva pileta del cliente.");
                        return true;
                    }
                    else //CORRECCIÓN: ESTA CONDICIÓN ESTÁ MAL, SI LA PRIMERA SI EL DNI ESTÁ TERCERO EN LA LISTA, EN LA PRIMERA Y SEGUNDA ITERACIÓN DEL CICLO LO AGREGARÍA COMO NUEVO CLIENTE
                    {
                        Cliente nuevoCliente = new Cliente();
                        nuevoCliente.DNI = dni;
                        nuevoCliente.CodigoPiletaCliente = codigoPileta;
                        nuevoCliente.FechaInstalacion = DateTime.Today;
                        //CORRECCIÓN: SE DEBE RETORNAR AMBOS VALORES A LA VEZ, ADEMÁS UN MÉTODO DEBE TENER UN ÚNICO TIPO DE DATOS A RETORNAR, ES DECIR, NO PUEDO RETORNAR UN BOOL Y LUEGO UN STRING.
                        //ADEMÁS NO SE USA EL VALOR DEL MÉTODO DevolverNota
                        Personas.Add(nuevoCliente);
                        DevolverNota("Se dio de alta la venta y el cliente, recuerde que debe completar sus datos.");
                        return true;

                    }
                   
                }
                
            }
            return false;
            
        }

    }
}
