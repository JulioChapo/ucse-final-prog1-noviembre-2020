﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica_Final
{
    public abstract class Pileta
    {
        public int CodigoPileta { get; set; }
        public decimal CantidadDeLitros { get; set; }
        public decimal Precio { get; set; }

        public Colores Color { get; set; }

        public enum Colores
        {
            Azul, Celeste, Gris
        }

        //CORRECCIÓN: ESTO DEBERIA SER UN MÉTODO VIRTUAL QUE DEVUELVA 0 POR DEFECTO, DE ESA FORMAS LAS SUBCLASES QUE NO LO NECESITEN NO DEBEN IMPLEMENTAR EL MÉTODO.
        public abstract decimal CalcularPorcentajeDescuento();
    }
}
