﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica_Final
{
    class PiletaLona: Pileta
    {
        public decimal Alto { get; set; }
        public decimal Ancho { get; set; }
        public decimal Profundidad { get; set; }
        public bool Filtro { get; set; }
        public bool CubrePiletas { get; set; }

        public override decimal CalcularPorcentajeDescuento()
        {
            return 8 * Precio / 100;
        }
    }
}
